Author: William Vera <billy@billy.com.mx>
Description: Fix hyphen-used-as-minus-sign in man pages.
--- a/arp-fingerprint.1
+++ b/arp-fingerprint.1
@@ -101,8 +101,8 @@ Display verbose progress messages.
 .B -o <option-string>
 Pass specified options to arp-scan. You need to enclose the options
 string in quotes if it contains spaces. e.g.
--o "-I eth1".  The commonly used options are --interface (-I) and --numeric
-(-N).
+\-o "\-I eth1".  The commonly used options are \--interface (\-I) and \--numeric
+(\-N).
 .SH EXAMPLES
 .nf
 $ arp-fingerprint 192.168.0.1
@@ -110,7 +110,7 @@ $ arp-fingerprint 192.168.0.1
 .fi
 .PP
 .nf
-$ arp-fingerprint -o "-N -I eth1" 192.168.0.202
+$ arp-fingerprint \-o "\-N \-I eth1" 192.168.0.202
 192.168.0.202 11110100000     FreeBSD 5.3, Win98, WinME, NT4, 2000, XP, 2003
 .fi
 .SH NOTES
--- a/arp-scan.1
+++ b/arp-scan.1
@@ -56,13 +56,13 @@ option.
 The target hosts to scan may be specified in one of three ways: by specifying
 the targets on the command line; by specifying a file containing the targets
 with the
-.B --file
+.B \--file
 option; or by specifying the
-.B --localnet
+.B \--localnet
 option which causes all possible hosts on the network attached to the interface
 (as defined by the interface address and mask) to be scanned. For hosts specified
 on the command line, or with the
-.B --file
+.B \--file
 option, you can use either IP addresses or hostnames.  You can also use network
 specifications
 .BR IPnetwork/bits ,
@@ -186,14 +186,14 @@ Outgoing ARP Packet Options
 =
 Field	Bits	Option	Notes
 =
-ar$hrd	16	--arphrd	Default is 1 (ARPHRD_ETHER)
-ar$pro	16	--arppro	Default is 0x0800
-ar$hln	8	--arphln	Default is 6 (ETH_ALEN)
-ar$pln	8	--arppln	Default is 4 (IPv4)
-ar$op	16	--arpop	Default is 1 (ARPOP_REQUEST)
-ar$sha	48	--arpsha	Default is interface h/w address
-ar$spa	32	--arpspa	Default is interface IP address
-ar$tha	48	--arptha	Default is zero (00:00:00:00:00:00)
+ar$hrd	16	\--arphrd	Default is 1 (ARPHRD_ETHER)
+ar$pro	16	\--arppro	Default is 0x0800
+ar$hln	8	\--arphln	Default is 6 (ETH_ALEN)
+ar$pln	8	\--arppln	Default is 4 (IPv4)
+ar$op	16	\--arpop	Default is 1 (ARPOP_REQUEST)
+ar$sha	48	\--arpsha	Default is interface h/w address
+ar$spa	32	\--arpspa	Default is interface IP address
+ar$tha	48	\--arptha	Default is zero (00:00:00:00:00:00)
 ar$tpa	32	None	Set to the target host IP address
 .TE
 .\" We need two paragraphs under the table to get the correct spacing.
@@ -225,9 +225,9 @@ Outgoing Ethernet Frame Options
 =
 Field	Bits	Option	Notes
 =
-Dest Address	48	--destaddr	Default is ff:ff:ff:ff:ff:ff
-Source Address	48	--srcaddr	Default is interface address
-Protocol Type	16	--prototype	Default is 0x0806
+Dest Address	48	\--destaddr	Default is ff:ff:ff:ff:ff:ff
+Source Address	48	\--srcaddr	Default is interface address
+Protocol Type	16	\--prototype	Default is 0x0806
 .TE
 .\" We need two paragraphs under the table to get the correct spacing.
 .PP
@@ -301,40 +301,40 @@ Where an option takes a value, that valu
 angle brackets. The letter indicates the type of data that is expected:
 .TP
 .B <s>
-A character string, e.g. --file=hostlist.txt.
+A character string, e.g. \--file=hostlist.txt.
 .TP
 .B <i>
 An integer, which can be specified as a decimal number or as a hexadecimal
-number if preceeded with 0x, e.g. --arppro=2048 or --arpro=0x0800.
+number if preceded with 0x, e.g. \--arppro=2048 or \--arpro=0x0800.
 .TP
 .B <f>
-A floating point decimal number, e.g. --backoff=1.5.
+A floating point decimal number, e.g. \--backoff=1.5.
 .TP
 .B <m>
 An Ethernet MAC address, which can be specified either in the format
 01:23:45:67:89:ab, or as 01-23-45-67-89-ab. The alphabetic hex characters
-may be either upper or lower case. E.g. --arpsha=01:23:45:67:89:ab.
+may be either upper or lower case. E.g. \--arpsha=01:23:45:67:89:ab.
 .TP
 .B <a>
-An IPv4 address, e.g. --arpspa=10.0.0.1
+An IPv4 address, e.g. \--arpspa=10.0.0.1
 .TP
 .B <h>
 Binary data specified as a hexadecimal string, which should not
 include a leading 0x. The alphabetic hex characters may be either
-upper or lower case. E.g. --padding=aaaaaaaaaaaa
+upper or lower case. E.g. \--padding=aaaaaaaaaaaa
 .TP
 .B <x>
 Something else. See the description of the option for details.
 .TP
-.B --help or -h
+.B \--help or \-h
 Display this usage message and exit.
 .TP
-.B --file=<s> or -f <s>
+.B \--file=<s> or \-f <s>
 Read hostnames or addresses from the specified file
 instead of from the command line. One name or IP
 address per line. Use "-" for standard input.
 .TP
-.B --localnet or -l
+.B \--localnet or \-l
 Generate addresses from network interface configuration.
 Use the network interface IP address and network mask
 to generate the list of target host addresses.
@@ -342,11 +342,11 @@ The list will include the network and br
 addresses, so an interface address of 10.0.0.1 with
 netmask 255.255.255.0 would generate 256 target
 hosts from 10.0.0.0 to 10.0.0.255 inclusive.
-If you use this option, you cannot specify the --file
+If you use this option, you cannot specify the \--file
 option or specify any target hosts on the command line.
 The interface specifications are taken from the
 interface that arp-scan will use, which can be
-changed with the --interface option.
+changed with the \--interface option.
 .TP
 .B --retry=<i> or -r <i>
 Set total number of attempts per host to <i>,
@@ -356,7 +356,7 @@ default=2.
 Set initial per host timeout to <i> ms, default=500.
 This timeout is for the first packet sent to each host.
 subsequent timeouts are multiplied by the backoff
-factor which is set with --backoff.
+factor which is set with \--backoff.
 .TP
 .B --interval=<x> or -i <x>
 Set minimum packet interval to <x>.
@@ -364,7 +364,7 @@ This controls the outgoing bandwidth usa
 the rate at which packets can be sent. The packet
 interval will be no smaller than this number.
 If you want to use up to a given bandwidth, then it is
-easier to use the --bandwidth option instead.
+easier to use the \--bandwidth option instead.
 The interval specified is in milliseconds by default,
 or in microseconds if "u" is appended to the value.
 .TP
@@ -376,7 +376,7 @@ per sec; and if you append "M" to the va
 units are megabits per second.
 The "K" and "M" suffixes represent the decimal, not
 binary, multiples. So 64K is 64000, not 65536.
-You cannot specify both --interval and --bandwidth
+You cannot specify both \--interval and \--bandwidth
 because they are just different ways to change the
 same underlying parameter.
 .TP
@@ -393,7 +393,7 @@ Display verbose progress messages.
 Use more than once for greater effect:
 .IP ""
 1 - Display the network address and mask used when the
---localnet option is specified, display any
+\--localnet option is specified, display any
 nonzero packet padding, display packets received
 from unknown hosts, and show when each pass through
 the list completes.
@@ -469,7 +469,7 @@ Set the source Ethernet MAC address to <
 This sets the 48-bit hardware address in the Ethernet
 frame header for outgoing ARP packets. It does not
 change the hardware address in the ARP packet, see
---arpsha for details on how to change that address.
+\--arpsha for details on how to change that address.
 The default is the Ethernet address of the outgoing
 interface.
 .TP
@@ -486,7 +486,7 @@ multicast address that they are listenin
 Use <m> as the ARP source Ethernet address
 This sets the 48-bit ar$sha field in the ARP packet
 It does not change the hardware address in the frame
-header, see --srcaddr for details on how to change
+header, see \--srcaddr for details on how to change
 that address. The default is the Ethernet address of
 the outgoing interface.
 .TP
@@ -619,7 +619,7 @@ using the network interface
 .IR eth0 .
 .PP
 .nf
-$ arp-scan --interface=eth0 192.168.0.0/24
+$ arp-scan \--interface=eth0 192.168.0.0/24
 Interface: eth0, datalink type: EN10MB (Ethernet)
 Starting arp-scan 1.4 with 256 hosts (http://www.nta-monitor.com/tools/arp-scan/)
 192.168.0.1     00:c0:9f:09:b8:db       QUANTA COMPUTER, INC.
@@ -656,7 +656,7 @@ eth0      Link encap:Ethernet  HWaddr 00
           TX packets:1542776 errors:0 dropped:0 overruns:0 carrier:0
           collisions:1644 txqueuelen:1000
           RX bytes:6184146 (5.8 MiB)  TX bytes:348887835 (332.7 MiB)
-# arp-scan --localnet
+# arp-scan \--localnet
 Interface: eth0, datalink type: EN10MB (Ethernet)
 Starting arp-scan 1.4 with 8 hosts (http://www.nta-monitor.com/tools/arp-scan/)
 10.0.84.179     00:02:b3:63:c7:57       Intel Corporation
--- a/get-iab.1
+++ b/get-iab.1
@@ -85,7 +85,7 @@ Display verbose progress messages.
 The default output file.
 .SH EXAMPLES
 .nf
-$ get-iab -v
+$ get-iab \-v
 Renaming ieee-iab.txt to ieee-iab.txt.bak
 Fetching IAB data from http://standards.ieee.org/regauth/oui/iab.txt
 Fetched 230786 bytes
--- a/get-oui.1
+++ b/get-oui.1
@@ -94,7 +94,7 @@ Display verbose progress messages.
 The default output file.
 .SH EXAMPLES
 .nf
-$ get-oui -v
+$ get-oui \-v
 Renaming ieee-oui.txt to ieee-oui.txt.bak
 Fetching OUI data from http://standards.ieee.org/regauth/oui/oui.txt
 Fetched 1467278 bytes
